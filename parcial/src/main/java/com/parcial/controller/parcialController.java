package com.parcial.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.parcial.model.Alumno;
import com.parcial.model.Usuario;

@Controller
public class parcialController {
	//trabajando con git
	List<Alumno> lista = new ArrayList<Alumno>();
	Alumno aux;
	Usuario usuario;

	
	//comentando desde la rama nicolas
	@GetMapping
	public String met1(Model model) {
		usuario = new Usuario();
		model.addAttribute("usuario", usuario);
		return "login";
	}
	
	@PostMapping("/ingresar")
	public String met2(@ModelAttribute Usuario usuario) {
		String resultado="login";
		if(usuario.validar()) {
			resultado="plantilla";
		}
		
		
		
		return resultado;
	}
	
	@GetMapping("/inicio")
	public String met3(Model model) {
		return "inicio";
	}
	
	@GetMapping("/salir")
	public String met5(Model model) {
		return "redirect:/";
	}
	
	@GetMapping("/alumnos")
	public String met4(Model model) {
		model.addAttribute("alumno", lista);
		model.addAttribute("usuario", usuario);
		return "alumnos";
	}
	
	//********* AGREGAR ****************** 
	@GetMapping("/add")
	public String formu2(Model modelo) {
		 Alumno alu = new Alumno();
		 modelo.addAttribute("alumno",alu);
		 return "add";
	 }
	//********* GUARDAR ******************

	@PostMapping("/guardar")
	public String Formu3(@ModelAttribute Alumno alumno) {
		aux = buscar(alumno.getLu());
		if(aux==null) {
		lista.add(alumno);
		}else {
			int pos=lista.indexOf(aux);
			lista.set(pos, alumno);
		}
	return "redirect:/alumnos";
	}
	//********* EDITAR ******************
	@GetMapping("/edit/{lu}")
	public String formu4(@PathVariable("lu") int lu, Model model) {		 
		 Alumno alu = buscar(lu);		 
		 model.addAttribute("alumno",alu);		 
		 return "add";
	 }

	//********* ELIMINAR ******************
	@GetMapping("/del/{lu}")
	public String formu5(@PathVariable("lu") int lu, Model model) {
		 
		 Alumno alu = buscar(lu);
		 lista.remove(alu);
		 model.addAttribute("alu",lista);
		 
		 return "redirect:/alumnos";
	}
	//********* BUSCAR ******************
	public Alumno buscar(int lu) {
		Alumno alu = null;
		for(Alumno a : lista) {
		   if (a.getLu()==lu) {
			   alu=a;
		   }
		} 		
		return alu;
	}
	
	
}
