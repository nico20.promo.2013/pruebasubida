package com.parcial.model;

public class Alumno {
	private int lu;
	private String ayn;
	private int nota1;
	private int nota2;
	private int nota3;
	double promedio;
	
	public Alumno(int lu, String ayn, int nota1, int nota2, int nota3) {
		this.lu = lu;
		this.ayn = ayn;
		this.nota1 = nota1;
		this.nota2 = nota2;
		this.nota3 = nota3;
		
	}
	public Alumno() {
	}
	public int getLu() {
		return lu;
	}
	public void setLu(int lu) {
		this.lu = lu;
	}
	public String getAyn() {
		return ayn;
	}
	public void setAyn(String ayn) {
		this.ayn = ayn;
	}
	public int getNota1() {
		return nota1;
	}
	public void setNota1(int nota1) {
		this.nota1 = nota1;
	}
	public int getNota2() {
		return nota2;
	}
	public void setNota2(int nota2) {
		this.nota2 = nota2;
	}
	public int getNota3() {
		return nota3;
	}
	public void setNota3(int nota3) {
		this.nota3 = nota3;
	}
	
	public double promedio() {
		
		promedio=(this.nota1+this.nota2+this.nota3)/3;
		return promedio;
	}
	
	public String condicion() {
		String resultado=null;
		if(this.promedio<6) {
			resultado="Desaprobado";
		}else if(this.promedio<8) {
			resultado="Aprobado";
		}else {
			resultado="Promocionado";
		}
		return resultado;
	}
	
	
}
