package com.parcial.model;

public class Usuario {
	private String nombre;
	private String clave;
	
	public Usuario(String nombre, String clave) {
		this.nombre = nombre;
		this.clave = clave;
	}

	public Usuario() {
	
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}
	
	public boolean validar() {
		boolean res = false;
		if( clave.compareTo("123456")==0 && nombre.compareTo("admin")==0) {
			res=true;
		}
		return res;
	}
}
